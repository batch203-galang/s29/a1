/*
Instructions s29 Activity:
1. Create an activity.js file on where to write and save the solution for the activity.
2. Find users with the letter “s” in their first name or d in their last name.
a. Use the $or operator.
b. Show only the firstName and lastName fields and hide the _id field.
3. Find users who are from the HR department and their age is greater than or equal to 70.
a. Use the $and operator
4. Find users with the letter e in their first name and have an age of less than or equal to 30.
a. Use the $and, $regex and $lte operators
5. Create a git repository named S29.
6. Initialize a local git repository, add the remote link and push to git with the commit message of Add activity code.
7. Add the link in Boodle.
*/


db.users.find(
	{
		$or: [
		{firstName:{$regex: 's', $options: '$i'}},
		{lastName: {$regex: 'd', $options: '$i'}},
		]
	},
	{
		firstName: 1,
		lastName: 1,
		_id: 0
	}
);

//
db.users.find({
	$and:[
		{age: {$gte: 70}},
		{department: "HR"}
		]
	}
);

//
db.users.find({
	$and:[
		{firstName:{$regex: 'e', $options: '$i'}},
		{age: {$lte: 30}}
		]
	}
);